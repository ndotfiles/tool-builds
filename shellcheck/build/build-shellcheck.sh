#!/usr/bin/env bash

set -x
set -e

source util.sh

shellcheck_version=0.4.7

wget_archive_verified \
  https://github.com/koalaman/shellcheck/archive/v$shellcheck_version.zip \
  861bece99c0bdec5b870bd70cc064ab72deb5fe28a50f28774f19549f31ea6af

cd shellcheck-$shellcheck_version
cabal install
