wget_verified() {
  url=$1
  expected_sha=$2
  url_file="$(basename "$url")"
  wget "$url"
  ls "$url_file" >/dev/null || return 1
  actual_sha="$(shasum -a 256 $url_file)"
  actual_sha=($actual_sha)
  actual_sha="${actual_sha[0]}"
  if [[ "$actual_sha" == "$expected_sha" ]]; then
    echo "SHA-256 for $url_file matched the expected $expected_sha"
    return 0
  else
    echo "SHA-256 for $url_file is $actual_sha not the expected $expected_sha!"
    return 1
  fi
}

wget_archive_verified() {
  url="$1"
  expected_sha="$2"
  url_file="$(basename $url)"
  wget "$url"
  ls "$url_file" >/dev/null
  rm -rf wget_archive_verify
  mkdir wget_archive_verify
  extension="${url_file##*.}"
  if [ "$extension" = "zip" ]; then
    unzip "$url_file" -d wget_archive_verify >/dev/null
  elif [ "$extension" = "gz" ]; then
    tar xf "$url_file" -C wget_archive_verify
  fi
  if check_dir_sha256 wget_archive_verify "$expected_sha"; then
    mv wget_archive_verify/* .
    rm "$url_file"
    rmdir wget_archive_verify
  else
    return 1
  fi
}

dir_sha256() {
  dir_sha="$(cd $1 &&
    find . -type f -print0 | LC_ALL=C sort -z |
    xargs -0 shasum -a 256 | shasum -a 256)"
  dir_sha=($dir_sha)
  echo "${dir_sha[0]}"
}

check_dir_sha256() {
  dir="$1"
  expected_sha="$2"
  actual_sha="$(dir_sha256 $dir)"
  if [[ "$actual_sha" == "$expected_sha" ]]; then
    echo "SHA-256 of $dir contents is $actual_sha as expected."
    return 0
  else
    echo "SHA-256 for $dir is $actual_sha not the expected $expected_sha"
    return 1
  fi
}
