#!/usr/bin/env bash

set -x
set -e

source util.sh

tmux_version=2.6

wget_verified \
  https://github.com/tmux/tmux/releases/download/$tmux_version/tmux-$tmux_version.tar.gz \
  b17cd170a94d7b58c0698752e1f4f263ab6dc47425230df7e53a6435cc7cd7e8

tar xf tmux-${tmux_version}.tar.gz
cd tmux-${tmux_version}
./configure --prefix=/root
make
make install
