#!/usr/bin/env bash

# This currently does not work on Docker as AppImage requires FUSE. See:
# https://github.com/AppImage/AppImageKit/wiki/fuse
# Instead, I just build nvim on my Debian VM.

set -x
set -e

mkdir -p /tmp/neovim-build
cd /tmp/neovim-build

nvim_version=0.3.1

# Turn off dir SHA checking for now.
# nvim_dir_sha256=f9977ffd17c150550e1764eaf818e77ea486b29ceecf3ac8a1bbaeda6ebd0607

# The nvim appimage generation script assumes it's operating in a git repo, so
# we need to actually clone the neovim git repo rather than pulling an archive.
git clone https://github.com/neovim/neovim
cd neovim
git checkout "v$nvim_version"

# Move .git folder out temporarily to make the SHA-256 stable
mv .git ..
# check_dir_sha256 . "$nvim_dir_sha256"
mv ../.git .

make appimage-nightly

# Copy the .AppImage file to a standard name.
cp ./build/bin/*.AppImage ../nvim

# Upload to GCS
cd /tmp/neovim-build

util=nvim
echo "SHA-256 of $util at version $nvim_version is $(shasum -a 256 "$util")"
gzip "$util"

read -p "Upload to GCS ndotfiles bucket (y/n)? " -n 1 -r REPLY
echo
if [[ $REPLY =~ ^[Yy]$ ]]; then
  gsutil cp "$util.gz" gs://ndotfiles fi
