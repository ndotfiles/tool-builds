#!/usr/bin/env bash

set -x
set -e

source util.sh

vendored_crates_commit=538fc2d950f6

wget https://bitbucket.org/draffensperger/vendored-crates/get/$vendored_crates_commit.zip

unzip "$vendored_crates_commit.zip"
mv "draffensperger-vendored-crates-$vendored_crates_commit" vendored-crates

check_dir_sha256 vendored-crates \
  067d6bc34b62149adfe97daa3cf9ce0dc2f77e6a8f4166b5661d6f0dbc556453

cd vendored-crates/alacritty

cargo build --release
