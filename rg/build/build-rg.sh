#!/usr/bin/env bash

set -x
set -e

source util.sh

rg_version=0.8.0

wget_archive_verified \
  https://github.com/BurntSushi/ripgrep/archive/$rg_version.tar.gz \
  ecdf0e52d71d2d89c7c3ba88e5648971517ceea52683a9ef63a49fd0a0ddf7de

# This makes the binary be in a consistent location
mv ripgrep-$rg_version ripgrep

cd ripgrep

# This is a generic build
# cargo build --release

# This build depends on having a recent Intel CPU and a Rust nightly release
RUSTFLAGS="-C target-cpu=native" cargo build --release \
    --features 'simd-accel avx-accel'
