#!/usr/bin/env bash

set -x
set -e

haskell_version=8.2.2
haskell_archive=haskell-platform-$haskell_version-unknown-posix--core-x86_64.tar.gz

wget https://haskell.org/platform/download/$haskell_version/$haskell_archive
touch $haskell_archive

haskell_archive_sha256=bd01bf2b34ea3d91b1c82059197bb2e307e925e0cb308cb771df45c798632b58

# This will exit the script if the SHA-256 does not match due to the 
# set -e above.
echo "$haskell_archive_sha256  $haskell_archive" | shasum -c -a 256

tar xf $haskell_archive

./install-haskell-platform.sh

cabal update

# This is required fro Debian stretch
# See https://www.haskell.org/platform/#linux-generic
settings_file=/usr/local/haskell/ghc-$haskell_version-x86_64/lib/ghc-$haskell_version/settings
sed -i 's/C compiler supports -no-pie\"\,\s*\"NO\"/C compiler supports -no-pie\"\,\"YES\"/' "$settings_file"
